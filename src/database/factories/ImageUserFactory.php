<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ImageUser;
use Faker\Generator as Faker;

$factory->define(ImageUser::class, function (Faker $faker) {
    return [
        'image_name' => random_int(1, 999),
    ];
});
