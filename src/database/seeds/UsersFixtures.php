<?php

declare(strict_types=1);

use App\ImageUser;
use App\User;
use Illuminate\Database\Seeder;

class UsersFixtures extends Seeder
{
    public function run(): void
    {
        factory(User::class, 40000)->create()->each(function (User $u) {
            $u->images()->save(factory(ImageUser::class)->make());
            $u->images()->save(factory(ImageUser::class)->make());
            $u->images()->save(factory(ImageUser::class)->make());
            $u->images()->save(factory(ImageUser::class)->make());
            $u->images()->save(factory(ImageUser::class)->make());
            $u->images()->save(factory(ImageUser::class)->make());
            $u->images()->save(factory(ImageUser::class)->make());
        });
    }
}
