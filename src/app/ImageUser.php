<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use \Illuminate\Database\Eloquent\Builder;

class ImageUser extends Model
{
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function getByUsrEmail(string $email): Builder
    {
        return $this->whereHas('user', function (Builder $query) use($email){
            $query->where('users.email', "=",  $email);
        });
    }
}
