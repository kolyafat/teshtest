<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImageUserRequest;
use App\ImageUser;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private const PER_PAGE = 8;

    /**
     * @var ImageUser
     */
    private $image;

    public function __construct(ImageUser $image)
    {
        $this->image = $image;
    }

    public function getUserImages(ImageUserRequest $request): LengthAwarePaginator
    {
        if (null === $request->getFilteredUserEmail()) {

            return $this->image->with('user')->paginate(self::PER_PAGE);
        }

        return $this->image
            ->getByUsrEmail($request->getFilteredUserEmail())
            ->with('user')
            ->paginate(self::PER_PAGE);
    }
}
