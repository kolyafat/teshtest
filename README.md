## Running:

- `docker-compose build`
- `docker-compose up -d`
- `docker-compose run --rm composer update`
- `docker-compose run --rm php php artisan migrate`
- `docker-compose run --rm php php artisan db:seed`
- `docker-compose run --rm npm run dev`
